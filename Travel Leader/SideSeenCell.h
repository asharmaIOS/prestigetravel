//
//  SideSeenCell.h
//  Travel Leader
//
//  Created by Gurpreet's on 21/09/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideSeenCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UILabel *seennamelbl;

@end
