//
// AppDelegate.h
// Travel Leader
//
// Created by Gurpreet Singh on 3/31/17.
// Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property(nonatomic, assign) int launchTag;

@property(nonatomic, strong) NSString *BASEAPI;
@property(nonatomic, strong) NSString *BASEAPIPNR;
@property(nonatomic, strong) NSString *BASEURLNOTIFDETAILS;



+ (AppDelegate *)sharedAppDelegate;

@end
